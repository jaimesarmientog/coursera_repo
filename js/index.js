$(function(){
	$("[data-toggle='tooltip']").tooltip();
	$("[data-toggle='popover']").popover();
	$('.carousel').carousel({
		interval:2000}
	);
	$('#suscripcion').on('show.bs.modal',function (e){
		console.log('el modal se está mostrando');
		$('#suscribirbtn').addClass('btn2').prop('disabled',true);
	});
	$('#suscripcion').on('shown.bs.modal',function (e){
		console.log('el modal se mostó');
	});
	$('#suscripcion').on('hide.bs.modal',function (e){
		console.log('el modal se está ocultando');
	$('#suscribirbtn').removeClass('btn2').prop('disabled',false);
	});
	$('#suscripcion').on('hidden.bs.modal',function (e){
		console.log('el modal se ocultó');
	});
});